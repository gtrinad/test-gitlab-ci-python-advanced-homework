import random

from factory import Factory, LazyAttribute, LazyFunction
from factory.fuzzy import FuzzyText, FuzzyInteger
from faker import Factory as FakerFactory

from module_29_testing.hw.main.models import Client, Parking

faker = FakerFactory.create()


class ClientFactory(Factory):
    class Meta:
        model = Client

    name = LazyAttribute(lambda _: faker.first_name())
    surname = LazyAttribute(lambda _: faker.last_name())
    credit_card = FuzzyText(length=16)
    car_number = FuzzyText(length=10)


class ParkingFactory(Factory):
    class Meta:
        model = Parking

    address = LazyAttribute(lambda _: faker.street_address())
    opened = LazyFunction(lambda: random.choice([True, False]))
    count_places = FuzzyInteger(10, 100)
    count_available_places = LazyAttribute(lambda obj: obj.count_places)
